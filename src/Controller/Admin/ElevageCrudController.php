<?php

namespace App\Controller\Admin;

use App\Entity\Elevage;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ElevageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Elevage::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('nom', 'Nom'),
            IntegerField::new('numero_siret', 'Numéro de Siret'),
            DateTimeField::new('createdAt', 'Créé le'),
            TextEditorField::new('presentation', 'Présentation'),
            AssociationField::new('user', 'Créé par'),
            AssociationField::new('photos', 'Photos'),
        ];
    }
}
