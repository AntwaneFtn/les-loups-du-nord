<?php

namespace App\Controller;

use App\Entity\Annonce;
use App\Repository\AnnonceRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AnnonceController extends AbstractController
{
    /**
     * @Route("/annonce", name="annonce")
     */
    public function index(AnnonceRepository $annonceRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $annonces = $annonceRepository->findAll();

        $pagination = $paginator->paginate(
            $annonces, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            2 /*limit per page*/
        );

        $pagination->setCustomParameters([
            'align' => 'center', # center|right (for template: twitter_bootstrap_v4_pagination and foundation_v6_pagination)
            'size' => 'large', # small|large (for template: twitter_bootstrap_v4_pagination)
            'style' => 'bottom',
            'span_class' => 'whatever',
        ]);
    
        return $this->render('annonce/index.html.twig', [
            'annonces' => $annonces,
            'pagination' => $pagination
        ]);
    }

    /**
    * @Route("/annonce/{id}/show", name="annonce_show")
    */
    public function show(Annonce $annonce): Response
    {

        if(!$annonce->getStatut()){
            return $this->redirectToRoute('annonce_index');
        }

        return $this->render("annonce/show.html.twig",[
            'annonce'=>$annonce,
        ]);
    }

//     /**
//    * @Route("/annonce/{id}/contact", name="contact")
//    */
//    public function contact(Annonce $annonce, Request $request): Response
//    {
//        $message = $request->query->get('contenu');
//
//         //////////////////ENVOI EMAIL VALIDATION/////////////////////////
//        //Avec MailDev
//        $transport=new \Swift_SmtpTransport($_ENV["SERVEURSMTPMAILDEV"],$_ENV["PORTSMTPMAILDEV"]);
//        //Avec MailTrap
//        // $transport=new \Swift_SmtpTransport($_ENV["SERVEURSMTPMAILTRAP"],$_ENV["PORTSMTPMAILTRAP"]);
//        // $transport->setUsername($_ENV["USERSMTPMAILTRAP"]);
//        // $transport->setPassword($_ENV["PASSWORDSMTPMAILTRAP"]);
//
//        $mailer=new \Swift_Mailer($transport);
//
//        $mail=new \Swift_Message("Contact pour une annonce");
//        $mail->setFrom('lesloupsdunord@gmail.com');
//        $mail->setTo(['admin@gmail.com']);
//        $mail->setBody(
//            $this->renderView('annonce/_messagecontact.html.twig',[
//                'message'=> $message
//            ]),
//            'text/html'
//        );
//
//        try {
//            $mailer->send($mail);
//            $this->addFlash('success',"<h4h>Votre message a bien été envoyé</h4>");
//        } catch (\Swift_TransportException $e) {
//            $this->addFlash("danger",$e->getMessage());
//        }
//
//         return $this->redirectToRoute('annonce');
//    }
}
