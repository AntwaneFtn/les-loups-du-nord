<?php

namespace App\Entity;

use App\Repository\PedigreeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PedigreeRepository::class)
 */
class Pedigree
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=Animal::class, inversedBy="pere", cascade={"persist", "remove"})
     */
    private $mere;

    /**
     * @ORM\OneToOne(targetEntity=Animal::class, inversedBy="pere", cascade={"persist", "remove"})
     */
    private $pere;

    /**
     * @ORM\OneToOne(targetEntity=Animal::class, inversedBy="grand_pere_paternel", cascade={"persist", "remove"})
     */
    private $grand_pere_paternel;

    /**
     * @ORM\OneToOne(targetEntity=Animal::class, inversedBy="grand_pere_maternel", cascade={"persist", "remove"})
     */
    private $grand_pere_maternel;

    /**
     * @ORM\OneToOne(targetEntity=Animal::class, inversedBy="grand_mere_paternelle", cascade={"persist", "remove"})
     */
    private $grand_mere_paternelle;

    /**
     * @ORM\OneToOne(targetEntity=Animal::class, inversedBy="grand_mere_maternelle", cascade={"persist", "remove"})
     */
    private $grand_mere_maternelle;



    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\OneToMany(targetEntity=Animal::class, mappedBy="pedigree")
     */
    private $animals;

    public function __construct()
    {
        $this->animals = new ArrayCollection();
    }

    public function __toString() {
        return $this->reference;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMere(): ?animal
    {
        return $this->mere;
    }

    public function setMere(?animal $mere): self
    {
        $this->mere = $mere;

        return $this;
    }

    public function getPere(): ?animal
    {
        return $this->pere;
    }

    public function setPere(?animal $pere): self
    {
        $this->pere = $pere;

        return $this;
    }

    public function getGrandPerePaternel(): ?animal
    {
        return $this->grand_pere_paternel;
    }

    public function setGrandPerePaternel(?animal $grand_pere_paternel): self
    {
        $this->grand_pere_paternel = $grand_pere_paternel;

        return $this;
    }

    public function getGrandPereMaternel(): ?animal
    {
        return $this->grand_pere_maternel;
    }

    public function setGrandPereMaternel(?animal $grand_pere_maternel): self
    {
        $this->grand_pere_maternel = $grand_pere_maternel;

        return $this;
    }

    public function getGrandMerePaternelle(): ?animal
    {
        return $this->grand_mere_paternelle;
    }

    public function setGrandMerePaternelle(?animal $grand_mere_paternelle): self
    {
        $this->grand_mere_paternelle = $grand_mere_paternelle;

        return $this;
    }

    public function getGrandMereMaternelle(): ?animal
    {
        return $this->grand_mere_maternelle;
    }

    public function setGrandMereMaternelle(?animal $grand_mere_maternelle): self
    {
        $this->grand_mere_maternelle = $grand_mere_maternelle;

        return $this;
    }


    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return Collection|Animal[]
     */
    public function getAnimals(): Collection
    {
        return $this->animals;
    }

    public function addAnimal(Animal $animal): self
    {
        if (!$this->animals->contains($animal)) {
            $this->animals[] = $animal;
            $animal->setPedigree($this);
        }

        return $this;
    }

    public function removeAnimal(Animal $animal): self
    {
        if ($this->animals->removeElement($animal)) {
            // set the owning side to null (unless already changed)
            if ($animal->getPedigree() === $this) {
                $animal->setPedigree(null);
            }
        }

        return $this;
    }

}
