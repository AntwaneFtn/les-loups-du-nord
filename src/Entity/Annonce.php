<?php

namespace App\Entity;

use App\Repository\AnnonceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AnnonceRepository::class)
 */
class Annonce
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prix;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $statut;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dispoAt;

    /**
     * @ORM\ManyToOne(targetEntity=Elevage::class, inversedBy="annonces")
     */
    private $elevage;

    /**
     * @ORM\ManyToOne(targetEntity=CategorieAnnonce::class)
     */
    private $categorie_annonce;

    /**
     * @ORM\OneToMany(targetEntity=Animal::class, mappedBy="annonce")
     */
    private $animals;

    /**
     * @ORM\ManyToMany(targetEntity=Photos::class, inversedBy="annonces")
     */
    private $photos;

    public function __construct()
    {
        $this->animals = new ArrayCollection();
        $this->photos = new ArrayCollection();
    }

    public function __toString() {
        return $this->titre;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(?int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getStatut(): ?bool
    {
        return $this->statut;
    }

    public function setStatut(?bool $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getDispoAt(): ?\DateTimeInterface
    {
        return $this->dispoAt;
    }

    public function setDispoAt(?\DateTimeInterface $dispoAt): self
    {
        $this->dispoAt = $dispoAt;

        return $this;
    }

    public function getElevage(): ?elevage
    {
        return $this->elevage;
    }

    public function setElevage(?elevage $elevage): self
    {
        $this->elevage = $elevage;

        return $this;
    }

    public function getCategorieAnnonce(): ?categorieAnnonce
    {
        return $this->categorie_annonce;
    }

    public function setCategorieAnnonce(?categorieAnnonce $categorie_annonce): self
    {
        $this->categorie_annonce = $categorie_annonce;

        return $this;
    }

    /**
     * @return Collection|Animal[]
     */
    public function getAnimals(): Collection
    {
        return $this->animals;
    }

    public function addAnimal(Animal $animal): self
    {
        if (!$this->animals->contains($animal)) {
            $this->animals[] = $animal;
            $animal->setAnnonce($this);
        }

        return $this;
    }

    public function removeAnimal(Animal $animal): self
    {
        if ($this->animals->removeElement($animal)) {
            // set the owning side to null (unless already changed)
            if ($animal->getAnnonce() === $this) {
                $animal->setAnnonce(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|photos[]
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(photos $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
        }

        return $this;
    }

    public function removePhoto(photos $photo): self
    {
        $this->photos->removeElement($photo);

        return $this;
    }
}
