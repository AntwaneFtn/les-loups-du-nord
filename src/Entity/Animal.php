<?php

namespace App\Entity;

use App\Repository\AnimalRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AnimalRepository::class)
 */
class Animal
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numero_lof;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numero_icad;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numero_puce;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numero_tatouage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $sexe;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="decimal", nullable=true)
     */
    private $taille;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $poids;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $categorie;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $bornAt;

    /**
     * @ORM\ManyToOne(targetEntity=Race::class, inversedBy="animals")
     */
    private $race;

    /**
     * @ORM\ManyToOne(targetEntity=Annonce::class, inversedBy="animals")
     */
    private $annonce;

    /**
     * @ORM\ManyToOne(targetEntity=Elevage::class, inversedBy="animals")
     */
    private $elevage;

    /**
     * @ORM\OneToOne(targetEntity=Pedigree::class, mappedBy="mere", cascade={"persist", "remove"})
     */
    private $mere;

    /**
     * @ORM\OneToOne(targetEntity=Pedigree::class, mappedBy="pere", cascade={"persist", "remove"})
     */
    private $pere;

    /**
     * @ORM\OneToOne(targetEntity=Pedigree::class, mappedBy="grand_pere_paternel", cascade={"persist", "remove"})
     */
    private $grand_pere_paternel;

    /**
     * @ORM\OneToOne(targetEntity=Pedigree::class, mappedBy="grand_pere_maternel", cascade={"persist", "remove"})
     */
    private $grand_pere_maternel;

    /**
     * @ORM\OneToOne(targetEntity=Pedigree::class, mappedBy="grand_mere_paternelle", cascade={"persist", "remove"})
     */
    private $grand_mere_paternelle;

    /**
     * @ORM\OneToOne(targetEntity=Pedigree::class, mappedBy="grand_mere_maternelle", cascade={"persist", "remove"})
     */
    private $grand_mere_maternelle;

    /**
     * @ORM\ManyToMany(targetEntity=Photos::class, inversedBy="animals")
     */
    private $photos;

    /**
     * @ORM\ManyToOne(targetEntity=Pedigree::class, inversedBy="animals")
     */
    private $pedigree;


    public function __construct()
    {
        $this->photos = new ArrayCollection();
    }

    public function __toString() {
        return $this->nom;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumeroLof(): ?string
    {
        return $this->numero_lof;
    }

    public function setNumeroLof(?string $numero_lof): self
    {
        $this->numero_lof = $numero_lof;

        return $this;
    }

    public function getNumeroIcad(): ?string
    {
        return $this->numero_icad;
    }

    public function setNumeroIcad(?string $numero_icad): self
    {
        $this->numero_icad = $numero_icad;

        return $this;
    }

    public function getNumeroPuce(): ?string
    {
        return $this->numero_puce;
    }

    public function setNumeroPuce(?string $numero_puce): self
    {
        $this->numero_puce = $numero_puce;

        return $this;
    }

    public function getNumeroTatouage(): ?string
    {
        return $this->numero_tatouage;
    }

    public function setNumeroTatouage(?string $numero_tatouage): self
    {
        $this->numero_tatouage = $numero_tatouage;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getSexe(): ?bool
    {
        return $this->sexe;
    }

    public function setSexe(?bool $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTaille(): ?int
    {
        return $this->taille;
    }

    public function setTaille(?int $taille): self
    {
        $this->taille = $taille;

        return $this;
    }

    public function getPoids(): ?string
    {
        return $this->poids;
    }

    public function setPoids(?string $poids): self
    {
        $this->poids = $poids;

        return $this;
    }

    public function getCategorie(): ?int
    {
        return $this->categorie;
    }

    public function setCategorie(?int $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getBornAt(): ?\DateTimeInterface
    {
        return $this->bornAt;
    }

    public function setBornAt(?\DateTimeInterface $bornAt): self
    {
        $this->bornAt = $bornAt;

        return $this;
    }

    public function getRace(): ?race
    {
        return $this->race;
    }

    public function setRace(?race $race): self
    {
        $this->race = $race;

        return $this;
    }

    public function getAnnonce(): ?annonce
    {
        return $this->annonce;
    }

    public function setAnnonce(?annonce $annonce): self
    {
        $this->annonce = $annonce;

        return $this;
    }

    public function getElevage(): ?elevage
    {
        return $this->elevage;
    }

    public function setElevage(?elevage $elevage): self
    {
        $this->elevage = $elevage;

        return $this;
    }

    public function getMere(): ?Pedigree
    {
        return $this->mere;
    }

    public function setMere(?Pedigree $mere): self
    {
        // unset the owning side of the relation if necessary
        if ($mere === null && $this->mere !== null) {
            $this->mere->setMere(null);
        }

        // set the owning side of the relation if necessary
        if ($mere !== null && $mere->getMere() !== $this) {
            $mere->setMere($this);
        }

        $this->mere = $mere;

        return $this;
    }

    public function getPere(): ?Pedigree
    {
        return $this->pere;
    }

    public function setPere(?Pedigree $pere): self
    {
        // unset the owning side of the relation if necessary
        if ($pere === null && $this->pere !== null) {
            $this->pere->setPere(null);
        }

        // set the owning side of the relation if necessary
        if ($pere !== null && $pere->getPere() !== $this) {
            $pere->setPere($this);
        }

        $this->pere = $pere;

        return $this;
    }

    public function getGrandPerePaternel(): ?Pedigree
    {
        return $this->grand_pere_paternel;
    }

    public function setGrandPerePaternel(?Pedigree $grand_pere_paternel): self
    {
        // unset the owning side of the relation if necessary
        if ($grand_pere_paternel === null && $this->grand_pere_paternel !== null) {
            $this->grand_pere_paternel->setGrandPerePaternel(null);
        }

        // set the owning side of the relation if necessary
        if ($grand_pere_paternel !== null && $grand_pere_paternel->getGrandPerePaternel() !== $this) {
            $grand_pere_paternel->setGrandPerePaternel($this);
        }

        $this->grand_pere_paternel = $grand_pere_paternel;

        return $this;
    }

    public function getGrandPereMaternel(): ?Pedigree
    {
        return $this->grand_pere_maternel;
    }

    public function setGrandPereMaternel(?Pedigree $grand_pere_maternel): self
    {
        // unset the owning side of the relation if necessary
        if ($grand_pere_maternel === null && $this->grand_pere_maternel !== null) {
            $this->grand_pere_maternel->setGrandPereMaternel(null);
        }

        // set the owning side of the relation if necessary
        if ($grand_pere_maternel !== null && $grand_pere_maternel->getGrandPereMaternel() !== $this) {
            $grand_pere_maternel->setGrandPereMaternel($this);
        }

        $this->grand_pere_maternel = $grand_pere_maternel;

        return $this;
    }

    public function getGrandMerePaternelle(): ?Pedigree
    {
        return $this->grand_mere_paternelle;
    }

    public function setGrandMerePaternelle(?Pedigree $grand_mere_paternelle): self
    {
        // unset the owning side of the relation if necessary
        if ($grand_mere_paternelle === null && $this->grand_mere_paternelle !== null) {
            $this->grand_mere_paternelle->setGrandMerePaternelle(null);
        }

        // set the owning side of the relation if necessary
        if ($grand_mere_paternelle !== null && $grand_mere_paternelle->getGrandMerePaternelle() !== $this) {
            $grand_mere_paternelle->setGrandMerePaternelle($this);
        }

        $this->grand_mere_paternelle = $grand_mere_paternelle;

        return $this;
    }

    public function getGrandMereMaternelle(): ?Pedigree
    {
        return $this->grand_mere_maternelle;
    }

    public function setGrandMereMaternelle(?Pedigree $grand_mere_maternelle): self
    {
        // unset the owning side of the relation if necessary
        if ($grand_mere_maternelle === null && $this->grand_mere_maternelle !== null) {
            $this->grand_mere_maternelle->setGrandMereMaternelle(null);
        }

        // set the owning side of the relation if necessary
        if ($grand_mere_maternelle !== null && $grand_mere_maternelle->getGrandMereMaternelle() !== $this) {
            $grand_mere_maternelle->setGrandMereMaternelle($this);
        }

        $this->grand_mere_maternelle = $grand_mere_maternelle;

        return $this;
    }

    /**
     * @return Collection|photos[]
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(photos $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
        }

        return $this;
    }

    public function removePhoto(photos $photo): self
    {
        $this->photos->removeElement($photo);

        return $this;
    }

    public function getPedigree(): ?Pedigree
    {
        return $this->pedigree;
    }

    public function setPedigree(?Pedigree $pedigree): self
    {
        $this->pedigree = $pedigree;

        return $this;
    }

}
