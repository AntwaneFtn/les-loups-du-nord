<?php

namespace App\Controller\Admin;

use App\Entity\Animal;
use App\Entity\Annonce;
use App\Entity\CategorieAnnonce;
use App\Entity\Elevage;
use App\Entity\LivreOr;
use App\Entity\Message;
use App\Entity\Pedigree;
use App\Entity\Photos;
use App\Entity\User;
use App\Entity\Projet;
use App\Entity\Race;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository=$userRepository;
    }

     /**
     * @Route("/admin_secret", name="admin")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function index(): Response
    {
        return $this->render('bo/dashboard.html.twig',[
            'nbUser'=>$this->userRepository->count([]),
            'users'=>$this->userRepository->findAll()
        ]);
    }


    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('<a href="/">Les loups du Nord</a>');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Projet', 'fas fa-list', Projet::class);
        yield MenuItem::section('Données utilisateurs');
        yield MenuItem::linkToCrud('Utilisateurs', 'fas fa-user-circle', User::class);
        yield MenuItem::section('Données élevage');
        yield MenuItem::linkToCrud('Elevage', 'fas fa-warehouse', Elevage::class);
        yield MenuItem::linkToCrud('Animal', 'fas fa-dog', Animal::class);
        yield MenuItem::linkToCrud('Race', 'fas fa-paw', Race::class);
        yield MenuItem::linkToCrud('Pédigrée', 'fas fa-project-diagram', Pedigree::class);
        yield MenuItem::section('Données annonce');
        yield MenuItem::linkToCrud('Annonce', 'fas fa-scroll', Annonce::class);
        yield MenuItem::linkToCrud('Categorie annonce', 'fas fa-certificate', CategorieAnnonce::class);
        yield MenuItem::section('Données livre or');
        yield MenuItem::linkToCrud('Livre or', 'fas fa-book', LivreOr::class);
        yield MenuItem::linkToCrud('Message', 'fas fa-comment-dots', Message::class);
        yield MenuItem::section('Images');
        yield MenuItem::linkToCrud('Photos', 'fas fa-images', Photos::class);
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);

    }
}
