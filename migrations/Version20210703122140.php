<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210703122140 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE animal (id INT AUTO_INCREMENT NOT NULL, race_id INT DEFAULT NULL, annonce_id INT DEFAULT NULL, elevage_id INT DEFAULT NULL, pedigree_id INT DEFAULT NULL, numero_lof VARCHAR(255) DEFAULT NULL, numero_icad VARCHAR(255) DEFAULT NULL, numero_puce VARCHAR(255) DEFAULT NULL, numero_tatouage VARCHAR(255) DEFAULT NULL, nom VARCHAR(255) DEFAULT NULL, sexe TINYINT(1) DEFAULT NULL, description LONGTEXT DEFAULT NULL, taille NUMERIC(10, 0) DEFAULT NULL, poids NUMERIC(10, 2) DEFAULT NULL, categorie INT DEFAULT NULL, born_at DATETIME DEFAULT NULL, INDEX IDX_6AAB231F6E59D40D (race_id), INDEX IDX_6AAB231F8805AB2F (annonce_id), INDEX IDX_6AAB231F4E2F28D (elevage_id), INDEX IDX_6AAB231FA0917A9C (pedigree_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE animal_photos (animal_id INT NOT NULL, photos_id INT NOT NULL, INDEX IDX_B28725058E962C16 (animal_id), INDEX IDX_B2872505301EC62 (photos_id), PRIMARY KEY(animal_id, photos_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE annonce (id INT AUTO_INCREMENT NOT NULL, elevage_id INT DEFAULT NULL, categorie_annonce_id INT DEFAULT NULL, titre VARCHAR(255) DEFAULT NULL, prix INT DEFAULT NULL, description LONGTEXT DEFAULT NULL, created_at DATETIME DEFAULT NULL, statut TINYINT(1) DEFAULT NULL, dispo_at DATETIME DEFAULT NULL, INDEX IDX_F65593E54E2F28D (elevage_id), INDEX IDX_F65593E516CC6183 (categorie_annonce_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE annonce_photos (annonce_id INT NOT NULL, photos_id INT NOT NULL, INDEX IDX_68D1E3C78805AB2F (annonce_id), INDEX IDX_68D1E3C7301EC62 (photos_id), PRIMARY KEY(annonce_id, photos_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categorie_annonce (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE elevage (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, nom VARCHAR(255) DEFAULT NULL, presentation LONGTEXT DEFAULT NULL, created_at DATETIME DEFAULT NULL, numero_siret INT DEFAULT NULL, UNIQUE INDEX UNIQ_7A23BDC0A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE elevage_photos (elevage_id INT NOT NULL, photos_id INT NOT NULL, INDEX IDX_19EF8D564E2F28D (elevage_id), INDEX IDX_19EF8D56301EC62 (photos_id), PRIMARY KEY(elevage_id, photos_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE livre_or (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, titre VARCHAR(255) DEFAULT NULL, contenu LONGTEXT DEFAULT NULL, annee INT DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, INDEX IDX_9557371CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, livre_or_id INT DEFAULT NULL, contenu LONGTEXT DEFAULT NULL, statut TINYINT(1) DEFAULT NULL, created_at DATETIME DEFAULT NULL, INDEX IDX_B6BD307FA76ED395 (user_id), INDEX IDX_B6BD307F1675FBD2 (livre_or_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pedigree (id INT AUTO_INCREMENT NOT NULL, mere_id INT DEFAULT NULL, pere_id INT DEFAULT NULL, grand_pere_paternel_id INT DEFAULT NULL, grand_pere_maternel_id INT DEFAULT NULL, grand_mere_paternelle_id INT DEFAULT NULL, grand_mere_maternelle_id INT DEFAULT NULL, reference VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_C36D0C7A39DEC40E (mere_id), UNIQUE INDEX UNIQ_C36D0C7A3FD73900 (pere_id), UNIQUE INDEX UNIQ_C36D0C7A6BC94379 (grand_pere_paternel_id), UNIQUE INDEX UNIQ_C36D0C7AEABCAB5 (grand_pere_maternel_id), UNIQUE INDEX UNIQ_C36D0C7AF7703A60 (grand_mere_paternelle_id), UNIQUE INDEX UNIQ_C36D0C7A45BFCC4 (grand_mere_maternelle_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE photos (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE projet (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, autoload TINYINT(1) DEFAULT NULL, description LONGTEXT DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE race (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, code VARCHAR(255) DEFAULT NULL, born_at DATETIME DEFAULT NULL, photo VARCHAR(255) DEFAULT NULL, userfirstname VARCHAR(255) DEFAULT NULL, rue VARCHAR(255) DEFAULT NULL, code_postal INT DEFAULT NULL, ville VARCHAR(255) DEFAULT NULL, numero VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE animal ADD CONSTRAINT FK_6AAB231F6E59D40D FOREIGN KEY (race_id) REFERENCES race (id)');
        $this->addSql('ALTER TABLE animal ADD CONSTRAINT FK_6AAB231F8805AB2F FOREIGN KEY (annonce_id) REFERENCES annonce (id)');
        $this->addSql('ALTER TABLE animal ADD CONSTRAINT FK_6AAB231F4E2F28D FOREIGN KEY (elevage_id) REFERENCES elevage (id)');
        $this->addSql('ALTER TABLE animal ADD CONSTRAINT FK_6AAB231FA0917A9C FOREIGN KEY (pedigree_id) REFERENCES pedigree (id)');
        $this->addSql('ALTER TABLE animal_photos ADD CONSTRAINT FK_B28725058E962C16 FOREIGN KEY (animal_id) REFERENCES animal (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE animal_photos ADD CONSTRAINT FK_B2872505301EC62 FOREIGN KEY (photos_id) REFERENCES photos (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE annonce ADD CONSTRAINT FK_F65593E54E2F28D FOREIGN KEY (elevage_id) REFERENCES elevage (id)');
        $this->addSql('ALTER TABLE annonce ADD CONSTRAINT FK_F65593E516CC6183 FOREIGN KEY (categorie_annonce_id) REFERENCES categorie_annonce (id)');
        $this->addSql('ALTER TABLE annonce_photos ADD CONSTRAINT FK_68D1E3C78805AB2F FOREIGN KEY (annonce_id) REFERENCES annonce (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE annonce_photos ADD CONSTRAINT FK_68D1E3C7301EC62 FOREIGN KEY (photos_id) REFERENCES photos (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE elevage ADD CONSTRAINT FK_7A23BDC0A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE elevage_photos ADD CONSTRAINT FK_19EF8D564E2F28D FOREIGN KEY (elevage_id) REFERENCES elevage (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE elevage_photos ADD CONSTRAINT FK_19EF8D56301EC62 FOREIGN KEY (photos_id) REFERENCES photos (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE livre_or ADD CONSTRAINT FK_9557371CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F1675FBD2 FOREIGN KEY (livre_or_id) REFERENCES livre_or (id)');
        $this->addSql('ALTER TABLE pedigree ADD CONSTRAINT FK_C36D0C7A39DEC40E FOREIGN KEY (mere_id) REFERENCES animal (id)');
        $this->addSql('ALTER TABLE pedigree ADD CONSTRAINT FK_C36D0C7A3FD73900 FOREIGN KEY (pere_id) REFERENCES animal (id)');
        $this->addSql('ALTER TABLE pedigree ADD CONSTRAINT FK_C36D0C7A6BC94379 FOREIGN KEY (grand_pere_paternel_id) REFERENCES animal (id)');
        $this->addSql('ALTER TABLE pedigree ADD CONSTRAINT FK_C36D0C7AEABCAB5 FOREIGN KEY (grand_pere_maternel_id) REFERENCES animal (id)');
        $this->addSql('ALTER TABLE pedigree ADD CONSTRAINT FK_C36D0C7AF7703A60 FOREIGN KEY (grand_mere_paternelle_id) REFERENCES animal (id)');
        $this->addSql('ALTER TABLE pedigree ADD CONSTRAINT FK_C36D0C7A45BFCC4 FOREIGN KEY (grand_mere_maternelle_id) REFERENCES animal (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE animal_photos DROP FOREIGN KEY FK_B28725058E962C16');
        $this->addSql('ALTER TABLE pedigree DROP FOREIGN KEY FK_C36D0C7A39DEC40E');
        $this->addSql('ALTER TABLE pedigree DROP FOREIGN KEY FK_C36D0C7A3FD73900');
        $this->addSql('ALTER TABLE pedigree DROP FOREIGN KEY FK_C36D0C7A6BC94379');
        $this->addSql('ALTER TABLE pedigree DROP FOREIGN KEY FK_C36D0C7AEABCAB5');
        $this->addSql('ALTER TABLE pedigree DROP FOREIGN KEY FK_C36D0C7AF7703A60');
        $this->addSql('ALTER TABLE pedigree DROP FOREIGN KEY FK_C36D0C7A45BFCC4');
        $this->addSql('ALTER TABLE animal DROP FOREIGN KEY FK_6AAB231F8805AB2F');
        $this->addSql('ALTER TABLE annonce_photos DROP FOREIGN KEY FK_68D1E3C78805AB2F');
        $this->addSql('ALTER TABLE annonce DROP FOREIGN KEY FK_F65593E516CC6183');
        $this->addSql('ALTER TABLE animal DROP FOREIGN KEY FK_6AAB231F4E2F28D');
        $this->addSql('ALTER TABLE annonce DROP FOREIGN KEY FK_F65593E54E2F28D');
        $this->addSql('ALTER TABLE elevage_photos DROP FOREIGN KEY FK_19EF8D564E2F28D');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F1675FBD2');
        $this->addSql('ALTER TABLE animal DROP FOREIGN KEY FK_6AAB231FA0917A9C');
        $this->addSql('ALTER TABLE animal_photos DROP FOREIGN KEY FK_B2872505301EC62');
        $this->addSql('ALTER TABLE annonce_photos DROP FOREIGN KEY FK_68D1E3C7301EC62');
        $this->addSql('ALTER TABLE elevage_photos DROP FOREIGN KEY FK_19EF8D56301EC62');
        $this->addSql('ALTER TABLE animal DROP FOREIGN KEY FK_6AAB231F6E59D40D');
        $this->addSql('ALTER TABLE elevage DROP FOREIGN KEY FK_7A23BDC0A76ED395');
        $this->addSql('ALTER TABLE livre_or DROP FOREIGN KEY FK_9557371CA76ED395');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FA76ED395');
        $this->addSql('DROP TABLE animal');
        $this->addSql('DROP TABLE animal_photos');
        $this->addSql('DROP TABLE annonce');
        $this->addSql('DROP TABLE annonce_photos');
        $this->addSql('DROP TABLE categorie_annonce');
        $this->addSql('DROP TABLE elevage');
        $this->addSql('DROP TABLE elevage_photos');
        $this->addSql('DROP TABLE livre_or');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE pedigree');
        $this->addSql('DROP TABLE photos');
        $this->addSql('DROP TABLE projet');
        $this->addSql('DROP TABLE race');
        $this->addSql('DROP TABLE user');
    }
}
