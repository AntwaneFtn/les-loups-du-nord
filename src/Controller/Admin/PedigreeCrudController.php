<?php

namespace App\Controller\Admin;

use App\Entity\Pedigree;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class PedigreeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Pedigree::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('reference', 'Référence'),
            AssociationField::new('mere', 'Mère'),
            AssociationField::new('pere', 'Père'),
            AssociationField::new('grand_mere_maternelle', 'Grand mère maternelle'),
            AssociationField::new('grand_pere_maternel', 'Grand père maternelle'),
            AssociationField::new('grand_mere_paternelle', 'Grand mère paternelle'),
            AssociationField::new('grand_pere_paternel', 'Grand père paternelle'),
        ];
    }
}
