<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BoController extends AbstractController
{

      /**
    * @Route("/bo", name=" bo_index")
    * @Security("is_granted('ROLE_ADMIN')")
    */
    public function bo(){
        return $this->render("bo/index.html.twig",[ ]);
    }

}