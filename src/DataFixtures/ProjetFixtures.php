<?php

namespace App\DataFixtures;

use DateTime;
use Faker\Factory;
use App\Entity\Race;
use App\Entity\User;
use App\Entity\Animal;
use App\Entity\Annonce;
use App\Entity\Photos;
use App\Entity\Projet;
use App\Entity\Elevage;
use App\Entity\LivreOr;
use App\Entity\Message;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProjetFixtures extends Fixture
{

     private $encoder;

     public function __construct(UserPasswordEncoderInterface $encoder)
     {
         $this->encoder = $encoder;
     }
 

    public function load(ObjectManager $manager)
    {


         /////////////////SUPRESSION FICHIER D'UN DOSSIER///////////////
        //  $repertoire = opendir("public/images/photos");      
        //  while (false !== ($fichier = readdir($repertoire))) {
        //      $chemin =  "public/images/photos/".$fichier;
        //      if ($fichier != ".." AND $fichier != "." AND !is_dir($fichier)){
        //          unlink($chemin); 
        //      }
        //  }
        //  closedir($repertoire); 
        //  if(file_exists("public/images/1.jpg")) copy("public/images/1.jpg","public/images/photos/1.jpg");
        //  if(file_exists("public/images/2.jpg")) copy("public/images/2.jpg","public/images/photos/2.jpg");
         ////////////////////////////////////////////////////////////////


        $faker = \Faker\Factory::create("fr_FR");

        $user1=new User();
        $user1->setUsername('Florent')
             ->setEmail('florent_fontaine@hotmail.com')
             ->setUserfirstname('Fontaine')
             ->setPhoto('user-1.jpg')
             ->setRoles(['ROLE_USER'])
             ->setPassword($this->encoder->encodePassword($user1,'petitponey'));
        $manager->persist($user1);


        $admin=new User();
        $admin->setUsername('Antoine')
             ->setEmail('antoine.fontaine.fr@gmail.com')
             ->setRoles(['ROLE_ADMIN'])
             ->setUserfirstname('Fontaine')
             ->setPhoto('user-2.jpg')
             ->setPassword($this->encoder->encodePassword($admin,'petitponey'));
        $manager->persist($admin);

        $photoElevage = New Photos();
        $photoElevage->setNom('femme-chien.jpg');
        $manager->persist($photoElevage);

        $elevage= new Elevage();
        $elevage->setUser($admin);
        $elevage->setNom('UN ÉLEVAGE DE POMSKY SITUÉ EN FRANCE');
        $elevage->setPresentation("<p>Installés dans la région de la Bretagne notre élevage est de type professionnel. Afin de pouvoir leur consacrer un maximum de temps et leur proposer des activités enrichissantes, nous souhaitons que nos chiens sont en nombre limité, et font partie intégrante de notre vie. Ils font entièrement partie de notre univers et sont au cœur de nos préoccupations. </p>
        <p>Ils disposent, d’un accès libre aux parties communes de la maison afin de profiter d’une vie agréable en famille, nous observer dans notre quotidien, mais aussi de structures aménagées pour eux, afin de se reposer, être en meute ou séparés, (par exemple durant des chaleurs lorsque nous ne souhaitons pas effectuer de saillie) et qui permettent aux mères de se mettre à l’écart du reste des autres chiens avec leurs chiots, si elles le souhaitent.</p>
        <p>Tous nos Pomsky s’entendent parfaitement bien et entretiennent des liens très forts entre eux, tout comme avec nous : ils constituent une meute soudée, complice et sont complémentaires. Ils y intègrent facilement de nouveaux individus. Nous les avons éduqués afin de ne pas se disputer une quelconque ressource alimentaire : les repas sont distribués une fois par jour, et les gamelles sont placées les unes à côté des autres.</p>");
        $elevage->addPhoto($photoElevage);
        $elevage->setCreatedAt(new \DateTime());

        $manager->persist($elevage);

        $projet1 = new Projet();
        $projet1->setName('Élevage');
        $projet1->setDescription("Nous espérons à travers notre élevage de Husky sibérien vous faire partager notre passion et y découvrir cette magnifique race de chien nordique ainsi que ses origines, son caractère et son standard. Vous pourrez également voir nos bébés Huskies à vendre et y trouver bien plus d’informations sur ce fantastique chien de compagnie.");
        $projet1->setImage('chien-fille.jpg');
        $manager->persist($projet1);

        $projet2 = new Projet();
        $projet2->setName('Pomsky');
        $projet2->setDescription("Le Pomsky est un chien de compagnie issu du croisement entre un Loulou de Poméranie et un Husky. Malgré sa récente naissance, il jouit d’ores et déjà d’une très grande popularité auprès des propriétaires canins. À ce jour, on distingue 4 types de Pomsky : les F1, F2 et F3, qui catégorisent les chiens d'après leur ascendance.");
        $projet2->setImage('2-pomsky.jpg');
        $manager->persist($projet2);

        $projet3 = new Projet();
        $projet3->setName('Husky');
        $projet3->setDescription("De taille moyenne, le Husky Sibérien est un chien à l’allure élégante et souple. Hyperactif, il sert traditionnellement de chien de traîneau. C'est un chien qui sait se montrer indépendant, mais il n’apprécie pas la solitude pour autant.");
        $projet3->setImage('2-siberian-husky.jpg');
        $manager->persist($projet3);

        $projet4 = new Projet();
        $projet4->setName('Chiens');
        $projet4->setDescription("Retrouvez dans cette rubrique toutes les informations précieuses pour mieux connaître le monde canin. Des astuces et conseils pratiques vous sont également prodigués sur l’adoption, l’alimentation, l’éducation pour bien vous occuper de votre chien.");
        $projet4->setImage('famille-chien.jpg');
        $manager->persist($projet4);

        $race1 = new Race();
        $race1->setNom('Pomsky');
        $manager->persist($race1);

        $race2 = new Race();
        $race2->setNom('Husky');
        $manager->persist($race2);

        $annonce1 = new Annonce();
        $annonce1->setElevage($elevage);
        $annonce1->setTitre('Nos petit Pomsky');
        $annonce1->setPrix(1200);
        $annonce1->setStatut(1);
        $annonce1->setDescription($faker->realText());
        $annonce1->setCreatedAt($faker->dateTimeBetween('-6 months'));
        $annonce1->setDispoAt($faker->dateTimeBetween('-4 months'));
        

        for ($i=1; $i <=6; $i++) { 

            $photo = New Photos();
            $photo->setNom('pomsky-'.$i.'.jpg');
            $manager->persist($photo);

            $pomsky = new Animal();
            $pomsky->setRace($race1);
            $pomsky->setElevage($elevage);
            $pomsky->setNom($faker->firstName());
            $pomsky->setDescription($faker->realText());
            $pomsky->setBornAt($faker->dateTimeBetween('-6 months'));
            $pomsky->setSexe($faker->boolean(50));
            $pomsky->setTaille($faker->randomFloat(2, 0, 15));
            $pomsky->setPoids($faker->randomFloat(2, 0, 15));
            $pomsky->setCategorie($faker->randomDigitNotNull());
            $pomsky->addPhoto($photo);
            $manager->persist($pomsky);

            $annonce1->addAnimal($pomsky);
            $annonce1->addPhoto($photo);
        }
        $manager->persist($annonce1);

        $annonce2 = new Annonce();
        $annonce2->setElevage($elevage);
        $annonce2->setTitre('Nos Husky');
        $annonce2->setPrix(1200);
        $annonce2->setStatut(1);
        $annonce2->setDescription($faker->realText());
        $annonce2->setCreatedAt($faker->dateTimeBetween('-6 months'));
        $annonce2->setDispoAt($faker->dateTimeBetween('-4 months'));
        

        for ($i=1; $i <=2; $i++) { 

            $photo = New Photos();
            $photo->setNom('husky-'.$i.'.jpg');
            $manager->persist($photo);

            $husky = new Animal();
            $husky->setRace($race2);
            $husky->setElevage($elevage);
            $husky->setNom($faker->firstName());
            $husky->setDescription($faker->realText());
            $husky->setBornAt($faker->dateTimeBetween('-6 months'));
            $husky->setSexe($faker->boolean(50));
            $husky->setTaille($faker->randomFloat(2, 0, 15));
            $husky->setPoids($faker->randomFloat(2, 0, 15));
            $husky->setCategorie($faker->randomDigitNotNull());
            $husky->addPhoto($photo);
            $manager->persist($husky);

            $annonce2->addAnimal($husky);
            $annonce2->addPhoto($photo);

        }

        $manager->persist($annonce2);

        $livreOr = new LivreOr();
        $livreOr->setUser($admin);
        $livreOr->setTitre('Livre d\'Or');
        $livreOr->setAnnee(2021);
        $livreOr->setImage('chien-livre.jpg');
        $livreOr->setContenu('Merci pour vos messages');
        $manager->persist($livreOr);

        $userMessage = '';

        for ($i=1; $i <=8; $i++) {
            $message= new Message();
            if($i%2==0){
                $userMessage = $admin;
            }else{
                $userMessage = $user1;
            }
            $message->setUser($userMessage);
            $message->setLivreOr($livreOr);
            $message->setContenu($faker->realText(130));
            $message->setCreatedAt($faker->dateTimeBetween('-6 months'));
            $message->setStatut($faker->boolean(50));
            $manager->persist($message);
        }

        

        $manager->flush();
    }

}
