<?php

namespace App\Controller\Admin;

use App\Entity\LivreOr;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class LivreOrCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return LivreOr::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('titre', 'Titre'),
            TextEditorField::new('contenu', 'Contenu'),
            IntegerField::new('annee', 'Année'),
            AssociationField::new('user', 'Créé par'),
            ImageField::new('image', 'Images')->setUploadDir("public/images/livre")->setBasePath('images/livre/'),
        ];
    }
}
