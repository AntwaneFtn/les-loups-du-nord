<?php

namespace App\Controller\Admin;

use App\Entity\Annonce;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class AnnonceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Annonce::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('elevage', 'Élevage'),
            TextField::new('titre', 'Titre'),
            AssociationField::new('animals', 'Animals'),
            TextEditorField::new('description', 'Description'),
            IntegerField::new('prix', 'Prix'),
            AssociationField::new('categorie_annonce', 'Catégorie'),
            DateTimeField::new('createdAt', 'Mise en ligne'),
            DateTimeField::new('dispoAt', 'Date de disponibilité'),
            AssociationField::new('photos', 'Photos'),
            BooleanField::new('statut', 'Validation'),
        ];
    }
}
