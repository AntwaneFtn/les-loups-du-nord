<?php

namespace App\Controller\Admin;

use App\Entity\CategorieAnnonce;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class CategorieAnnonceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return CategorieAnnonce::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('nom', 'Libellé'),
        ];
    }
}
