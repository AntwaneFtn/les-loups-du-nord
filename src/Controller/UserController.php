<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_index", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')",message="Vous devez être administrateur pour accéder à cette fonctionnalité")
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     * @Security("is_granted('ROLE_XXXXXX')",message="Cette fonctionnalité n'est pas disponible")
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
      * @Security("(is_granted('ROLE_USER') and user===user1) or is_granted('ROLE_ADMIN')",message="Vous devez être connecté et être le propriétaire du compte pour afficher les informations") 
     */
    public function show(User $user1): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user1,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
      * @Security("(is_granted('ROLE_USER') and user===user1) or is_granted('ROLE_ADMIN')",message="Vous devez être connecté et être le propriétaire du compte pour afficher les informations") 
     */
    public function edit(Request $request, User $user1): Response
    {

        $form = $this->createForm(UserType::class, $user1,[
            'currentUser'=>$this->getUser()
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $file = $form['fichierphoto']->getData();
            if($file){
                $fileName = uniqid().'.'.$file->guessExtension();
                try {
                    $file->move($this->getParameter('uploads_user'),$fileName); 
                    if ($user1->getPhoto() && file_exists($this->getParameter('uploads_user')."/".$user1->getPhoto() )){
                        unlink($this->getParameter('uploads_user')."/".$user1->getPhoto());
                    }
                    $user1->setPhoto($fileName);
                } catch (FileException $e) {
                    $this->addFlash('danger', " Votre photo n'a pas pu être envoyée ");
                }
            }

            $this->getDoctrine()->getManager()->flush();

            if (in_array('ROLE_ADMIN',$this->getUser()->getRoles())) {
                return $this->redirectToRoute('user_index');
            } else {
                return $this->redirectToRoute('user_show',['id'=>$this->getUser()->getId()]);
            }
            
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user1,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
    * @Security("is_granted('ROLE_ADMIN')",message="Vous devez être administrateur pour accéder à cette fonctionnalité")
     * 
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            // if(file_exists("images/photos/".$user->getPhoto()) && is_file("images/photos/".$user->getPhoto())){
            //     unlink("images/photos/".$user->getPhoto());
            //  }    
            // $entityManager = $this->getDoctrine()->getManager();
            // $entityManager->remove($user);
            // $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }
}
