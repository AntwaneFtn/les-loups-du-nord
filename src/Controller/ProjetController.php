<?php

namespace App\Controller;

use App\Repository\ElevageRepository;
use App\Repository\ProjetRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProjetController extends AbstractController
{
   
     /**
     * @Route("/", name="home")
     */
    public function home(ProjetRepository $projetRepository)
    {
        $projets = $projetRepository->findAll();
		return $this->render('projet/home.html.twig',[
            'projets' => $projets,
        ]);
    }

  

}
