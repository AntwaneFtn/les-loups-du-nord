<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/inscription", name="security_registration")
     */
    public function registration(Request $request,UserPasswordEncoderInterface $encoder,SessionInterface $session): Response
    {
        $user=new User();
        $form=$this->createForm(RegistrationType::class,$user);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $password=$user->getPassword();
            $hash=$encoder->encodePassword($user,$password);
            $user->setPassword($hash);
            
            $user->setRoles(["ROLE_USER"]);

            $code=uniqid();
            $user->setCode($code);
            $email=$user->getEmail();
            $user->setEmail($email.$this->generateRandomString());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager ->persist($user);

            //////////////////ENVOI EMAIL VALIDATION/////////////////////////
            if($_ENV["SERVEURSMTP"]=="maildev"){
                //Avec MailDev
                $transport=new \Swift_SmtpTransport($_ENV["SERVEURSMTPMAILDEV"],$_ENV["PORTSMTPMAILDEV"]);
            } 
            if($_ENV["SERVEURSMTP"]=="mailtrap"){
                //Avec MailTrap
                $transport=new \Swift_SmtpTransport($_ENV["SERVEURSMTPMAILTRAP"],$_ENV["PORTSMTPMAILTRAP"]);
                $transport->setUsername($_ENV["USERSMTPMAILTRAP"]);
                $transport->setPassword($_ENV["PASSWORDSMTPMAILTRAP"]);
            }
            $mailer=new \Swift_Mailer($transport);

            $message=new \Swift_Message("Confirmation Inscription Les Loups du Nord");
            $message->setFrom('admin@gmail.com');
            $message->setTo([$email=>$user->getUsername()]);
            $message->setBody(
                $this->renderView('security/_emailregistration.html.twig',[
                    'name'=> $user->getUserfirstname().' '.$user->getUsername(),
                    'code'=>$user->getCode()
                ]),
                'text/html'
            );

            try {
                $mailer->send($message);
                $this->addFlash('success','<h4>Vous allez recevoir un mail de confirmation, cliquez sur le lien pour valide votre inscription </h4>');
                $entityManager ->flush();
            } catch (\Swift_TransportException $e) {     
                $this->addFlash('danger','<h4>Echec inscription inscription </h4>');         
                $this->addFlash("danger",$e->getMessage());
                return $this->redirectToRoute("security_registration");
            }

            ///////////////////////////////////////////////////////////////

            return $this->redirectToRoute("security_login");

        }


        return $this->render('security/registration.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/connexion", name="security_login")
     */
    public function login()
    {


        return $this->render('security/login.html.twig', [
            
        ]);

    }

     /**
     * @Route("/deconnexion", name="security_logout")
     */
    public function logout()
    {
        
    }

    /**
     * @Route("/validation/{code}", name="security_validation")
     */
    public function validation($code, UserRepository $userRepository){

        $user=$userRepository->findOneBy(['code'=>$code]);

        if ($user) {
           $user->setCode('');
           $email=substr($user->getEmail(),0,strlen($user->getEmail())-10);
           $user->setEmail($email);
           $entityManager=$this->getDoctrine()->getManager();
           $entityManager->persist($user);
           $entityManager->flush();
           $this->addFlash("success","Validation  réussie");
        }else{
            $this->addFlash("danger","Echec validation");
        }
        
        return $this->render('security/login.html.twig');
    }

    /**
     * @Route("/mdpoublie", name="user_mdpoublie")
     */
    public function mdpoublie(Request $request,UserRepository $userRepository){

        $email=$request->request->get('email');
        $user=$userRepository->findOneBy(['email' =>$email]);

        if($user){
            $this->mailreinitmdp($user,'mdpoublie');
        }else{
            $this->addFlash('danger',"Cet email n'existe pas, veuillez vous inscrire");
        }
        return $this->redirectToRoute('security_login');
        
    } 

    /**
    * @Route("/valid/{code}/{mode}", name="user_valid")
    */
    public function user_valid($code,$mode,UserRepository $userRepository,Request $request,UserPasswordEncoderInterface $encoder){
        
        $user=$userRepository->findOneBy( ['code' => $code]);
    
        if ($user) {    
            
            if($request->request->count()>0){  
               
                $password=$request->request->get('password');
                $passwordconfirm=$request->request->get('passwordconfirm');     
                if ($mode=="mdpreinit") {      
                    $oldpassword= $request->request->get('oldpassword');
                    if (!$encoder->isPasswordValid($user,$oldpassword)) {
                        $this->addFlash('danger',"Votre ancien mot de passe n'était pas celui-ci");
                        return $this->redirectToRoute('user_valid',[
                            'code'=>$code,
                            'mode'=>$mode
                        ]); 
                    } 
                    if ($oldpassword==$password) {
                        $this->addFlash('danger',"Votre nouveau mot de passe est identique à l'ancien Modifiez le ");
                        return $this->redirectToRoute('user_valid',[
                            'code'=>$code,
                            'mode'=>$mode
                        ]); 
                    }   
                }   
                
                if($password==$passwordconfirm){
                 
                    $hash=$encoder->encodePassword($user,$password);
                    $user->setPassword($hash);
                   
                    $user->setCode('');
                 
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($user);
                    $entityManager->flush();
                    
                    $this->addFlash('success',"Votre mot de passe a été réinitialisé avec succès");
                    
                    return $this->redirectToRoute('security_login');          
                }else{
                   
                    $this->addFlash('danger',"Vos mots de passe ne sont pas identiques , veuillez les saisir à nouveau");
                }        
            }
        }else{
            $this->addFlash('danger',"Ce code n'existe pas");
            return $this->redirectToRoute('security_login');  
        }
	    
        return $this->render('security/validation.html.twig',[
            'mode'=>$mode
        ]);
    }

    /**
     * @Route("/user/{id}/mdpreinit",name="user_mdpreinit")
      * @Security("is_granted('ROLE_USER') and user===user1",message="Vous devez être connecté pour réinitaliser votre mot de passe ")  
     */
    function user_mdpreinit(User $user1){
        $this->mailreinitmdp($user1,'mdpreinit');
        return ($this->render('user/show.html.twig',[
            'user'=>$user1
        ]));
    }

    function mailreinitmdp($user,$mode){

        $code=uniqid();
        $user->setCode($code);
        $email=$user->getEmail();
  
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);

        if($_ENV["SERVEURSMTP"]=="maildev"){
            //Avec MailDev
            $transport=new \Swift_SmtpTransport($_ENV["SERVEURSMTPMAILDEV"],$_ENV["PORTSMTPMAILDEV"]);
        } 
        if($_ENV["SERVEURSMTP"]=="mailtrap"){
            //Avec MailTrap
            $transport=new \Swift_SmtpTransport($_ENV["SERVEURSMTPMAILTRAP"],$_ENV["PORTSMTPMAILTRAP"]);
            $transport->setUsername($_ENV["USERSMTPMAILTRAP"]);
            $transport->setPassword($_ENV["PASSWORDSMTPMAILTRAP"]);
        }
        $mailer=new \Swift_Mailer($transport);
        $message = new \Swift_Message('REINITIALISATION MOT DE PASSE LES LOUPS DU NORD');
        $message ->setFrom('admin@gmail.fr');
        $message ->setTo([$email => $user->getUsername()]);
        $message ->setBody(
                $this->renderView(
                    'security/_emailreinit.html.twig',[
                        'name' => $user->getUsername(),
                        'code'=>$code,
                        'mode'=>$mode
                    ]
                ),
                'text/html'
        );
        try {
            $mailer->send($message);
            $this->addFlash('success','<h4>Vous allez recevoir un mail pour réinitialiser le mot de passe</h4>');
            $entityManager ->flush();
        }
        catch (\Swift_TransportException $e) {
            $this->addFlash('danger','<h4>Votre réinitialisation du mot de passe a échoué </h4>');
            $this->addFlash('danger',$e->getMessage());
        }
        
        }

    //Fonction qui génère une chaîne de caractères aléatoire
    /**
    * @return string
    */
    function generateRandomString($longueur = 10)
    {
        $caracteres = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_/|*-+';
        $longueurMax = strlen($caracteres);
        $chaineAleatoire = '';
        for ($i = 0; $i < $longueur; $i++){
            $chaineAleatoire .= $caracteres[rand(0, $longueurMax - 1)];
        }
        return $chaineAleatoire;
    }

}
