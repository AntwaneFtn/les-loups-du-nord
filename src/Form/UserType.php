<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $user=$options['currentUser'];

        $builder
            ->add('email')
            //->add('password')
            ->add('username')
            ->add('userfirstname')
            ->add('numero')
            ->add('rue')
            ->add('codePostal')
            ->add('ville')
            ->add('bornAt',DateType::class,[
                'widget'=>'single_text',
                'required' => false
            ])
            ->add('fichierphoto', FileType::class, [
                'mapped' => false,
                'label' => 'Upload fichier',
                'required' => false,
                'constraints' => [              
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => ['image/jpeg'],
                        'mimeTypesMessage' => 'SVP Uploadez un fichier JPEG valide',
                    ])
                ],
            ])

        ;

        if (in_array('ROLE_ADMIN',$user->getRoles())) {
            $builder
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'Admin' => 'ROLE_ADMIN',
                    'User' => 'ROLE_USER',
                ],
                'expanded'  => true, // liste déroulante
                'multiple'  => true, // choix multiple
            ])
            ->add('code')
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'currentUser'=>null
        ]);
    }
}
