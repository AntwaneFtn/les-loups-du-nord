<?php

namespace App\Controller;

use DateTime;
use App\Entity\LivreOr;
use App\Entity\Message;
use App\Form\MessageType;
use App\Repository\LivreOrRepository;
use App\Repository\MessageRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LivreOrController extends AbstractController
{
    /**
     * @Route("/livre_or", name="livre_or")
     */
    public function index(MessageRepository $messageRepository, LivreOrRepository $livreOrRepository): Response
    {
        $date = new DateTime();
        $year = $date->format('Y');
        $message=new Message();
        $form=$this->createForm(MessageType::class,$message);

        $messages = $messageRepository->findBy(array(), array('createdAt' => 'DESC'));
        $livreOr = $livreOrRepository->findOneBy(['annee' => $year]);
        return $this->render('livre_or/index.html.twig', [
            'messages' => $messages,
            'livreOr' => $livreOr,
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/livre_or/{id}/message/new", name="new_message")
     */
    public function newMessage(LivreOr $livreOr, Request $request): Response
    {

        $message=new Message();
        $form=$this->createForm(MessageType::class,$message);

        $form->handleRequest($request);

        if($form->isSubmitted()&& $form->isValid()){

            $message->setUser($this->getUser());
            $message->setLivreOr($livreOr);
            $message->setCreatedAt(new \DateTime());
            $message->setStatut(0);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager ->persist($message);

//            //////////////////ENVOI EMAIL VALIDATION/////////////////////////
//             //Avec MailDev
//            $transport=new \Swift_SmtpTransport($_ENV["SERVEURSMTPMAILDEV"],$_ENV["PORTSMTPMAILDEV"]);
//            //Avec MailTrap
//            // $transport=new \Swift_SmtpTransport($_ENV["SERVEURSMTPMAILTRAP"],$_ENV["PORTSMTPMAILTRAP"]);
//            // $transport->setUsername($_ENV["USERSMTPMAILTRAP"]);
//            // $transport->setPassword($_ENV["PASSWORDSMTPMAILTRAP"]);
//
//            $mailer=new \Swift_Mailer($transport);
//
//            $mail=new \Swift_Message("Un nouveau message sur votre site");
//            $mail->setFrom('lesloupsdunord@gmail.com');
//            $mail->setTo(['admin@gmail.com']);
//            $mail->setBody(
//                $this->renderView('livre_or/_messagevalidation.html.twig',[
//                    'message'=> $message
//                ]),
//                'text/html'
//            );

            try {
//                $mailer->send($mail);
                $entityManager->flush();
                $this->addFlash('success',"<h4h>Merci ! Votre message a bien été envoyé</h4>");
            } catch (\Swift_TransportException $e) {              
                $this->addFlash("danger",$e->getMessage());
                $entityManager->remove($message);
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('livre_or');
    }

    /**
     * @Route("/livre_or/message/{id}/edit", name="edit_message")
     */
    public function editMessage(Message $message, Request $request): Response
    {

            $message->setStatut(0);
            $message->setContenu($request->query->get('contenu'));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager ->persist($message);

            //////////////////ENVOI EMAIL VALIDATION/////////////////////////
             //Avec MailDev
            $transport=new \Swift_SmtpTransport($_ENV["SERVEURSMTPMAILDEV"],$_ENV["PORTSMTPMAILDEV"]);
            //Avec MailTrap
            // $transport=new \Swift_SmtpTransport($_ENV["SERVEURSMTPMAILTRAP"],$_ENV["PORTSMTPMAILTRAP"]);
            // $transport->setUsername($_ENV["USERSMTPMAILTRAP"]);
            // $transport->setPassword($_ENV["PASSWORDSMTPMAILTRAP"]);

            $mailer=new \Swift_Mailer($transport);

            $mail=new \Swift_Message("Un message a été modifié sur votre site");
            $mail->setFrom('lesloupsdunord@gmail.com');
            $mail->setTo(['admin@gmail.com']);
            $mail->setBody(
                $this->renderView('livre_or/_messagevalidation.html.twig',[
                    'message'=> $message
                ]),
                'text/html'
            );

            try {
                $mailer->send($mail);
                $entityManager->flush();
                $this->addFlash('success',"<h4h>Merci ! Votre message a bien été modifié</h4>");
            } catch (\Swift_TransportException $e) {              
                $this->addFlash("danger",$e->getMessage());
                $entityManager->remove($message);
                $entityManager->flush();
            }

        return $this->redirectToRoute('livre_or');
    }


     /**
    * @Route("/livre_or/message/{id}/delete", name="delete_message")
    */
    public function deleteMessage(Request $request,Message $message)
    {   

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($message);       
        $entityManager->flush();

        $this->addFlash('success',"Le message a bien été supprimé");

        return $this->redirectToRoute('livre_or');
    }
}
