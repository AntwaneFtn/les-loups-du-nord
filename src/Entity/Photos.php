<?php

namespace App\Entity;

use App\Repository\PhotosRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PhotosRepository::class)
 */
class Photos
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\ManyToMany(targetEntity=Elevage::class, mappedBy="photos")
     */
    private $elevages;

    /**
     * @ORM\ManyToMany(targetEntity=Annonce::class, mappedBy="photos")
     */
    private $annonces;

    /**
     * @ORM\ManyToMany(targetEntity=Animal::class, mappedBy="photos")
     */
    private $animals;

    public function __construct()
    {
        $this->elevages = new ArrayCollection();
        $this->annonces = new ArrayCollection();
        $this->animals = new ArrayCollection();
    }

    public function __toString() {
        return $this->nom;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|Elevage[]
     */
    public function getElevages(): Collection
    {
        return $this->elevages;
    }

    public function addElevage(Elevage $elevage): self
    {
        if (!$this->elevages->contains($elevage)) {
            $this->elevages[] = $elevage;
            $elevage->addPhoto($this);
        }

        return $this;
    }

    public function removeElevage(Elevage $elevage): self
    {
        if ($this->elevages->removeElement($elevage)) {
            $elevage->removePhoto($this);
        }

        return $this;
    }

    /**
     * @return Collection|Annonce[]
     */
    public function getAnnonces(): Collection
    {
        return $this->annonces;
    }

    public function addAnnonce(Annonce $annonce): self
    {
        if (!$this->annonces->contains($annonce)) {
            $this->annonces[] = $annonce;
            $annonce->addPhoto($this);
        }

        return $this;
    }

    public function removeAnnonce(Annonce $annonce): self
    {
        if ($this->annonces->removeElement($annonce)) {
            $annonce->removePhoto($this);
        }

        return $this;
    }

    /**
     * @return Collection|Animal[]
     */
    public function getAnimals(): Collection
    {
        return $this->animals;
    }

    public function addAnimal(Animal $animal): self
    {
        if (!$this->animals->contains($animal)) {
            $this->animals[] = $animal;
            $animal->addPhoto($this);
        }

        return $this;
    }

    public function removeAnimal(Animal $animal): self
    {
        if ($this->animals->removeElement($animal)) {
            $animal->removePhoto($this);
        }

        return $this;
    }
}
