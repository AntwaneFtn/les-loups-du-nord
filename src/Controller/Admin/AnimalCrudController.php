<?php

namespace App\Controller\Admin;

use App\Entity\Animal;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class AnimalCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Animal::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('annonce', 'Annonce'),
            AssociationField::new('elevage', 'Élévage'),
            AssociationField::new('race', 'Race'),
            TextField::new('nom', 'Nom'),
            ChoiceField::new('sexe', 'Sexe')->setChoices(['Mâle' => 1, 'Femelle' => 0]),
            DateTimeField::new('bornAt', 'Né.e le'),
            TextEditorField::new('description', 'Description'),
            NumberField::new('taille', 'Taille'),
            NumberField::new('poids', 'Poids'),
            IntegerField::new('categorie', 'Catégorie'),
            AssociationField::new('pedigree', 'Pédigree'),
            AssociationField::new('photos', 'Photos'),
            TextField::new('numero_lof', 'Numéro LOF'),
            TextField::new('numero_icad', 'Numéro ICAD'),
            TextField::new('numero_puce', 'Numéro puce'),
            TextField::new('numero_tatouage', 'Numéro tatouage'),
        ];
    }
}
