<?php

namespace App\Controller;

use App\Repository\ElevageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ElevageController extends AbstractController
{
    /**
     * @Route("/elevage", name="elevage")
     */
    public function index(ElevageRepository $elevageRepository): Response
    {
        $elevages = $elevageRepository->findAll();
        return $this->render('elevage/index.html.twig', [
            'controller_name' => 'ElevageController',
            'elevages' => $elevages
        ]);
    }
}
