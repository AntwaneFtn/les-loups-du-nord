<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            
            TextField::new('username', 'Nom'),
            TextField::new('userfirstname', 'Prénom'),
            TextField::new('email', 'Email'),
            ArrayField::new('roles', 'Roles'),
            DateField::new('bornAt', 'Date de naissance'),
            TextField::new('rue', 'Rue'),
            IntegerField::new('code_postal', 'Code postal'),
            TextField::new('ville', 'Ville'),
            TextField::new('numero', 'Numéro'),
            ImageField::new('photo', 'Photo de profil')->setUploadDir("public/images/user")->setBasePath('images/user/')
        ];
    }

    public function configureActions(Actions $actions): Actions
{
    return $actions
        ->remove(Crud::PAGE_INDEX, Action::NEW)
    ;
}


    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
